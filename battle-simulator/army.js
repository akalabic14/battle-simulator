import Squad from './squad'
import configurable from './configurable'
import configuration from './configuration'

const {methods} = configuration

class Army {
    constructor(attack_strategy) {
        this.squads = [];
        for(var i=0; i<configurable.squads_per_army; i++) {
            this.squads.push(new Squad(attack_strategy))
        }
    }

    get squads() {
        return this._squads
    }

    set squads(squads) {
        this._squads = squads
    }

    attack(opponents) {
        var all_squads = []
        var squads_hash = {};
        var attacks_array = [];
        opponents.forEach(function(army, i) {
            army.squads.forEach(function(squad, j) {
                squads_hash[squad] = {army: army, index: j}
                all_squads.push(squad)
            })
        })
        this.squads.forEach(function(squad) {
            let choice;
            switch(squad.attack_strategy) {
                case 'random':
                    choice = methods.random(0, all_squads.length)
                    break;
                case 'weakest':
                    choice = all_squads.indexOf(this.sortSquads(all_squads)[0])
                    break;
                case 'strongest':
                    choice = all_squads.indexOf(this.sortSquads(all_squads)[all_squads.length - 1])
                    break;
            }
            attacks_array.push(squad.attack(all_squads[choice])
            .then(function(response) {
                if(all_squads[choice].isDestroyed()) {
                    let opponent_army = squads_hash[all_squads[choice]]
                    opponents_army.army.squad.splice(opponent_army.index, 1)
                }
            }))
        }, this);
        return Promise.all(attacks_array)
    }

    isDestroyed() {
        return this.squads.length > 0
    }

    war(opponents) {
        this.attack(opponents)
            .then(function(response) {
                opponents.forEach(function(opponent, i) {
                    if (opponent.isDestroyed()) {
                        opponents.splice(i, 1)
                    }
                    if (opponents.length > 0) {
                        war(opponents)
                    } else {
                        return 'Victory!'
                    }
                })
            })
    }

    sortSquads(squads) {
        return squads.sort(
            (squad_1, squad_2) => {
                if(squad_1.getTotalSquadHealth() > squad_2.getTotalSquadHealth()) {
                    return 1
                }
                if(squad_1.getTotalSquadHealth() < squad_2.getTotalSquadHealth()) {
                    return -1
                }
                if(squad_1.getSquadsExperiance() > squad_2.getSquadsExperiance()) {
                    return 1
                }
                if(squad_1.getSquadsExperiance() < squad_2.getSquadsExperiance()) {
                    return -1
                }
                if(squad_1.units.length > squad_2.units.length) {
                    return 1
                }
                if(squad_1.units.length < squad_2.units.length) {
                    return -1
                }
                if(squad_1.getTotalSquadDamage() > squad_2.getTotalSquadDamage()) {
                    return 1
                }
                if(squad_1.getTotalSquadDamage() < squad_2.getTotalSquadDamage()) {
                    return -1
                }
                return 0
            }
        )
    }
}
export default Army