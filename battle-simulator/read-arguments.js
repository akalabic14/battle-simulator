import configuration from './configuration'

const {errorMessages, reading_arguments} = configuration
/**
     * @description
     * Function that checks if all arguments are valid
     * 
     * @param {args}
     * Array of command line arguments
     * 
     * @param @returns {true}
     * True if all of the arguments are valid
     * 
*/
export default {
    read : (arguments_json) => {
        for (var name in reading_arguments.arguments) {
            if (name == 'attack_strategy' || arguments_json.hasOwnProperty(name)) {
                let argument_config = reading_arguments.arguments[name]
                switch(reading_arguments.arguments[name]['type']) {
                    case 'number': 
                        if (typeof arguments_json[name] != 'number') {
                            throw new Error(`Wrong argument type for ${name}: ${errorMessages.expected('number', typeof arguments_json[name])}`)
                        }
                        if (argument_config.min && arguments_json[name] < argument_config.min) {
                            throw new Error(`${errorMessages.with(name)} ${errorMessages.min} : ${errorMessages.expected(`greater then ${argument_config.min}`, arguments_json[name])}`)
                        } 
                        if (argument_config.max && arguments_json[name] > argument_config.max) {
                            throw new Error(`${errorMessages.with(name)} ${errorMessages.max} : ${errorMessages.expected(`less then ${argument_config.max}`, arguments_json[name])}`)
                        }
                        arguments_json[name] = arguments_json[name]
                        break
                    case "array":
                        if (Array.isArray(arguments_json[name])) {
                            for(let i=0, value; i<arguments_json[name].length; i++) {
                                value = arguments_json[name][i]
                                if (typeof value != reading_arguments.arguments['attack_strategy']['type']) {
                                    throw new Error(`Wrong argument type for attack strategy: ${errorMessages.expected(reading_arguments.arguments['attack_strategy']['type'], typeof value)}`)
                                } else if (reading_arguments.arguments['attack_strategy'].options.indexOf(value) < 0) {
                                    throw new Error(`${errorMessages.with(name)} Invalid value. ${errorMessages.expected(reading_arguments.arguments['attack_strategy'].options.join('|'), value)}.`)
                                }
                            }
                        } else {
                            throw new Error(`${errorMessages.with(name)} Invalid value. ${errorMessages.expected('array', typeof arguments_json[name])}. ${errorMessages.readme(`${name} value.`)}`)
                        }
                        arguments_json[name] = arguments_json[name]
                        break
                    default:
                        break
                }
            } else {
                throw new Error(`Parameter ${name} not found. ${errorMessages.readme(`setting up configurable parameters.`)}`)
            }
        }    
        
        if (arguments_json['number_of_armies'] != arguments_json['attack_strategies'].length) {
            if (arguments_json['number_of_armies'] > arguments_json['attack_strategies'].length) {
                throw new Error(`${errorMessages.with('attack strategies')} Each army must have an attack strategy: ${errorMessages.expected(`${arguments_json['number_of_armies']}`, arguments_json['attack_strategies'].length)}`)
            } else {
                throw new Error(`${errorMessages.with('attack strategies')} More attack strategies than armies: ${errorMessages.expected(`${arguments_json['number_of_armies']}`, arguments_json['attack_strategies'].length)}`)
            }
        }

        return true
    }
}