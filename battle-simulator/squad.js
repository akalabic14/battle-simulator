import Unit from './unit'
import Soldier from './soldier'
import Vehicle from './vehicle'
import configuration from './configuration'
import configurable from './configurable'

const {methods, vehicles, units, errorMessages, soldiers, reading_arguments} = configuration

class Squad {
    constructor(attack_strategy) {
        this.attack_strategy = attack_strategy
        this.units = [];
        for(var i=0; i<configurable.units_per_squad; i++) {
            let soldier_or_unit = methods.random(0, 1) > 0 ? new Soldier(methods.random(units.recharge.min(true), units.recharge.max)) : new Vehicle(methods.random(units.recharge.min(false), units.recharge.max))
            this.units.push(soldier_or_unit)
        }
    }

    get attack_strategy() {
        return this._attack_strategy
    }

    set attack_strategy(attack_strategy) {
        if (typeof attack_strategy != reading_arguments.arguments['attack_strategy']['type']) {
            throw new Error(`Wrong argument type for attack strategy: ${errorMessages.expected(reading_arguments.arguments['attack_strategy']['type'], typeof attack_strategy)}`)
        } 
        if (reading_arguments.arguments.attack_strategy.options.indexOf(attack_strategy) < 0) {
            throw new Error(`${errorMessages.with('attack strategy')} Invalid value. ${errorMessages.expected(reading_arguments.arguments['attack_strategy'].options.join('|'), value)}.`)
        }

        this._attack_strategy = attack_strategy
    }

    get units() {
        return this._units
    }

    set units(units) {
        this._units = units
    }

    getAttackProbability() {
        return methods.gavg(this.units.map(unit => unit.getAttackProbability()))
    }

    getTotalSquadHealth() {
        return this.units.reduce((accumulator, unit) => accumulator + (unit instanceof Soldier) ? unit.health : unit.getRealHealth(), 0)
    }

    getSquadsExperiance() {
        return this.units.reduce((accumulator, unit) => accumulator + unit.expiriance, 0)
    }

    getTotalSquadDamage() {
        return this.units.reduce((accumulator, unit) => accumulator +  unit.getAttackingDamage(), 0)
    }

    isDestroyed() {
        return units.length > 0
    }

    attack(opponent) {
        var attack_array = []
        
        this.units.forEach(function(unit, index) {  
            if(opponent instanceof Soldier) {
                if (unit.getAttackProbability() > opponent.getAttackProbability()) {
                    attack_array.push(unit.attack(opponent));
                }
            } else {
                if(unit.getAttackProbability() < opponent.units[index % opponent.units.length]) {
                    attack_array.push(unit.attack(opponent.units[index % opponent.units.length]).then((response) => {
                        if (opponent.units[index % opponent.units.length].isDestroyed()) {
                            opponent.units = opponent.units.splice(index%opponent.units.length, 1)
                        }
                    }))
                }
            }
        }, this);
        return Promise.all(attack_array)
    }
}

export default Squad