import ReadingArguments from './read-arguments'
import configurable from './configurable'
import Army from './army'

console.log("Wellcome to Battle Simulator!")

ReadingArguments.read(configurable) 

console.log("Arguments successfully loaded.")
var armies = []
var wars = []
for(var i=0; i<configurable.number_of_armies; i++) {
    armies.push(new Army(configurable.attack_strategies[i]))
}
for(var i=0; i<configurable.number_of_armies; i++) {
    wars.push(new Promise(function(resolve) {
        armies[i].war(armies.splice(i, 1))
        .then(function(response) {
            if(response == 'Victory') {
                console.log(`Army ${i}. has won the war!`);
            }
        })
    }))
};
Promise.all(wars)
.then(function(response) {
    console.log('Peace has come.');
})