'use strict';

const chai = require('chai');
const Soldier = require('../dist/soldier').default;
const configuration = require('../dist/configuration');
const expect = chai.expect;
const errorMessages = configuration.default.errorMessages;
const soldiers_config = configuration.default.soldiers;
const units_config = configuration.default.units;


describe('Testing Soldier class methods', function () {
    it('Should return instanceof Soldier when initiated with correct values', function() {
        var test_soldier = new Soldier(units_config.recharge.min(true));
        expect(test_soldier instanceof Soldier).to.equal(true);
    });
    it('Expirience property should return minimum expirience when initiated with minimum expirience property', function() {
        var test_soldier = new Soldier(units_config.recharge.min(true));
        expect(test_soldier.experience).to.equal(soldiers_config.experience.min);
    });
    it(`getAttackingDamage method should return ${0.05 + (soldiers_config.experience.min / 100)} when soldier is just initiated`, function() {
        var test_soldier = new Soldier(units_config.recharge.min(true));
        var damage = test_soldier.getAttackingDamage();
        expect(damage).to.equal(0.05 + (soldiers_config.experience.min / 100));
    });
    it(`getAttackProbability should return value between ${0.5 * (1 + units_config.health.max/100) * (30 + soldiers_config.experience.min) / 100} and ${0.5 * (1 + units_config.health.max/100)}`, function() {
        var getAttackProbability = new Soldier(units_config.recharge.min(true)).getAttackProbability();
        var minAttackProbability = 0.5 * (1 + units_config.health.max/100) * (30 + soldiers_config.experience.min) / 100;
        var maxAttackProbability = 0.5 * (1 + units_config.health.max/100);
        expect(getAttackProbability >= minAttackProbability && getAttackProbability <= maxAttackProbability).to.equal(true);
    });
    it(`attack() should damage soldier_2 for soldier_1 attackingDamage if it resolves true, and not damage soldier_2 if it resolves false`, function(done) {
        var soldier_1 = new Soldier(units_config.recharge.min(true));
        var soldier_2 = new Soldier(units_config.recharge.min(true));

        var soldier_1_damage = soldier_1.getAttackingDamage();
        soldier_1.attack(soldier_2).then(function(response) {
            expect(soldier_2.health).to.equal(response ? units_config.health.max - soldier_1_damage : units_config.health.max);
            done();
        })
    })
    it(`Experience should be encreased for winning soldier after attack()`, function(done) {
        var soldier_1 = new Soldier(units_config.recharge.min(true));
        var soldier_2 = new Soldier(units_config.recharge.min(true));
        
        soldier_1.attack(soldier_2).then(function(response) {
            expect(soldier_1.experience).to.equal(response ?  soldiers_config.experience.min + 1  :  soldiers_config.experience.min);
            done();
        })
    })
});