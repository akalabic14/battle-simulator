'use strict';

const chai = require('chai');
const Unit = require('../dist/unit').default;
const configuration = require('../dist/configuration');
const expect = chai.expect;
const errorMessages = configuration.default.errorMessages;
const units_config = configuration.default.units;


describe('Testing Unit class methods', function () {
    it('Should return instanceof Unit when initiated with correct values', function() {
        var test_unit = new Unit(units_config.recharge.min(true));
        expect(test_unit instanceof Unit).to.equal(true);
    });
    it('Recharge property should return minimum recharge when initiated with minimum recharge property', function() {
        var test_unit = new Unit(units_config.recharge.min(true));
        expect(test_unit.recharge).to.equal(units_config.recharge.min(true));
    });
    it('Recharge property should throw error when initiated with less than minimum recharge property', function() {
        expect((function() {new Unit(units_config.recharge.min(true) - 1)})).to.throw(Error);
    });
    it('Recharge property should throw error when initiated with greater than maximum recharge property', function() {
        expect((function() {new Unit(units_config.recharge.max + 1)})).to.throw(Error);
    });
    it('Health property should return maximum health when initiated with correct values', function() {
        var test_unit = new Unit(units_config.recharge.min(true));
        expect(test_unit.health).to.equal(units_config.health.max);
    });
    it('takeDamage should reduce health for number provided to method', function() {
        var test_unit = new Unit(units_config.recharge.min(true));
        var old_health = test_unit.health;
        var damage = 1;
        test_unit.takeDamage(damage);
        expect(test_unit.health).to.equal(old_health - damage);
    })
    it('takeDamage should throw error when called with string value', function() {
        var test_unit = new Unit(units_config.recharge.min(true));
        expect((function() { test_unit.takeDamage('string')})).to.throw(Error);
    })
    it('takeDamage should throw error when called with boolean value', function() {
        var test_unit = new Unit(units_config.recharge.min(true));
        expect((function() { test_unit.takeDamage(true)})).to.throw(Error);
    })
    it('isDestroyed method should return false when called right after initialisation', function() {
        var test_unit = new Unit(units_config.recharge.min(true));
        expect(test_unit.isDestroyed()).to.equal(false);
    });
    it('isDestroyed method should return true when Unit is attacked with damage equal or bigger than its health', function() {
        var test_unit = new Unit(units_config.recharge.min(true));
        test_unit.takeDamage(test_unit.health);
        expect(test_unit.isDestroyed()).to.equal(true);
    });
});
