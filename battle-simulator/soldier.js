import Unit from './unit'
import configuration from './configuration'

const {soldiers, methods, errorMessages, units} = configuration

class Soldier extends Unit {
    
    constructor (recharge) {
        super(recharge)
        this.experience = soldiers.experience.min
    }

    get experience() {
        return this._experience
    }

    set experience(experience) {
        if (typeof experience != soldiers.experience['type']) {
            throw new Error(`Wrong argument type for experience: ${errorMessages.expected(soldiers.experience['type'], experience)}`)
        }
        if (experience < soldiers.experience.min) {
            throw new Error(`${errorMessages.with('experience')} ${errorMessages.min} : ${errorMessages.expected(`greater then ${soldiers.experience.min}`, experience)}`)
        }
        if (experience > soldiers.experience.max) {
            experience = soldiers.experience.max
        }
        this._experience = experience
    }
    
    getAttackProbability () {
        let probability = 0.5 * (1 + this.health/100) * methods.random(30 + this.experience, 100)
        return probability / 100
    }

    getAttackingDamage () {
        return (5 + this.experience)  / 100
    }

    attack(unit) {
        var that = this;
        console.log('Soldier attacking');
        return new Promise((resolve) => {
            let attacking_probability = that.getAttackProbability()
            let defending_probability = unit.getAttackProbability()
            console.log(`Attacking probability ${attacking_probability}`);
            console.log(`Defending probability ${defending_probability}`);
            if (attacking_probability > defending_probability) {
                let damage = that.getAttackingDamage()
                console.log(`Attack successful, unit damaged with ${damage}`)
                unit.takeDamage(damage)
                that.experience += 1
                setTimeout(() => {
                    resolve(true)
                }, that.recharge)
            } else {
                console.log('Attack failed')
                resolve(false);
            }
        })
    }
}

export default Soldier;