
    /**
     * @description
     * restrictions for arguments 
     */
const reading_arguments = {
    arguments: {
        number_of_armies: {
            type: 'number',
            min: 2
        },
        attack_strategies: {
            type: 'array',
            min: 2
        },
        attack_strategy: {
            type: 'string',
            options: ['random', 'weakest', 'strongest']
        },
        squads_per_army: {
            type: 'number',
            min: 2
        },
        units_per_squad: {
            type: 'number',
            min: 5,
            max: 10
        }
    },
},
/**
 * @description
 * Some generic error messages
 */
errorMessages = {
    readme: (got) => `Check README file for more details about ${got}`,
    expected: (exp, got) => `Expected ${exp}, but got ${got}`,
    min: 'value smaller than min allowed',
    max: 'value greater than max allowed',
    with: (got) => `Error with ${got}:`
},
/**
 * @description
 * restrictions for units arguments
 */
units = {
    health: {
        min: 0,
        max: 100,
        type: 'number'
    },
    recharge: {
        min: (soldier) => (soldier) ? 200 : 1000,
        max: 2000,
        type: 'number'      
    }
},
soldiers = {
    experience: {
        min: 0,
        max: 50,
        type: 'number'
    }
},

vehicles = {
    operators: {
        min: 1,
        max: 3
    }
},

methods = {
    random: (min, max) => Math.floor(Math.random() * (max - min) + min),
    avarage: (array) => array.reduce((a,b) => a + b, 0) / array.length,
    gavg: (array) => Math.pow(array.reduce((a, b) => a * b, 1), 1/array.length)
}

export default {reading_arguments, errorMessages, units, soldiers, methods, vehicles}