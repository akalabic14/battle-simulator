'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _unit = require('./unit');

var _unit2 = _interopRequireDefault(_unit);

var _soldier = require('./soldier');

var _soldier2 = _interopRequireDefault(_soldier);

var _vehicle = require('./vehicle');

var _vehicle2 = _interopRequireDefault(_vehicle);

var _configuration = require('./configuration');

var _configuration2 = _interopRequireDefault(_configuration);

var _configurable = require('./configurable');

var _configurable2 = _interopRequireDefault(_configurable);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var methods = _configuration2.default.methods,
    vehicles = _configuration2.default.vehicles,
    units = _configuration2.default.units,
    errorMessages = _configuration2.default.errorMessages,
    soldiers = _configuration2.default.soldiers,
    reading_arguments = _configuration2.default.reading_arguments;

var Squad = function () {
    function Squad(attack_strategy) {
        _classCallCheck(this, Squad);

        this.attack_strategy = attack_strategy;
        this.units = [];
        for (var i = 0; i < _configurable2.default.units_per_squad; i++) {
            var soldier_or_unit = methods.random(0, 1) > 0 ? new _soldier2.default(methods.random(units.recharge.min(true), units.recharge.max)) : new _vehicle2.default(methods.random(units.recharge.min(false), units.recharge.max));
            this.units.push(soldier_or_unit);
        }
    }

    _createClass(Squad, [{
        key: 'getAttackProbability',
        value: function getAttackProbability() {
            return methods.gavg(this.units.map(function (unit) {
                return unit.getAttackProbability();
            }));
        }
    }, {
        key: 'getTotalSquadHealth',
        value: function getTotalSquadHealth() {
            return this.units.reduce(function (accumulator, unit) {
                return accumulator + (unit instanceof _soldier2.default) ? unit.health : unit.getRealHealth();
            }, 0);
        }
    }, {
        key: 'getSquadsExperiance',
        value: function getSquadsExperiance() {
            return this.units.reduce(function (accumulator, unit) {
                return accumulator + unit.expiriance;
            }, 0);
        }
    }, {
        key: 'getTotalSquadDamage',
        value: function getTotalSquadDamage() {
            return this.units.reduce(function (accumulator, unit) {
                return accumulator + unit.getAttackingDamage();
            }, 0);
        }
    }, {
        key: 'isDestroyed',
        value: function isDestroyed() {
            return units.length > 0;
        }
    }, {
        key: 'attack',
        value: function attack(opponent) {
            var attack_array = [];

            this.units.forEach(function (unit, index) {
                if (opponent instanceof _soldier2.default) {
                    if (unit.getAttackProbability() > opponent.getAttackProbability()) {
                        attack_array.push(unit.attack(opponent));
                    }
                } else {
                    if (unit.getAttackProbability() < opponent.units[index % opponent.units.length]) {
                        attack_array.push(unit.attack(opponent.units[index % opponent.units.length]).then(function (response) {
                            if (opponent.units[index % opponent.units.length].isDestroyed()) {
                                opponent.units = opponent.units.splice(index % opponent.units.length, 1);
                            }
                        }));
                    }
                }
            }, this);
            return Promise.all(attack_array);
        }
    }, {
        key: 'attack_strategy',
        get: function get() {
            return this._attack_strategy;
        },
        set: function set(attack_strategy) {
            if ((typeof attack_strategy === 'undefined' ? 'undefined' : _typeof(attack_strategy)) != reading_arguments.arguments['attack_strategy']['type']) {
                throw new Error('Wrong argument type for attack strategy: ' + errorMessages.expected(reading_arguments.arguments['attack_strategy']['type'], typeof attack_strategy === 'undefined' ? 'undefined' : _typeof(attack_strategy)));
            }
            if (reading_arguments.arguments.attack_strategy.options.indexOf(attack_strategy) < 0) {
                throw new Error(errorMessages.with('attack strategy') + ' Invalid value. ' + errorMessages.expected(reading_arguments.arguments['attack_strategy'].options.join('|'), value) + '.');
            }

            this._attack_strategy = attack_strategy;
        }
    }, {
        key: 'units',
        get: function get() {
            return this._units;
        },
        set: function set(units) {
            this._units = units;
        }
    }]);

    return Squad;
}();

exports.default = Squad;