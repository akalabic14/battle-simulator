'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _unit = require('./unit');

var _unit2 = _interopRequireDefault(_unit);

var _configuration = require('./configuration');

var _configuration2 = _interopRequireDefault(_configuration);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var soldiers = _configuration2.default.soldiers,
    methods = _configuration2.default.methods,
    errorMessages = _configuration2.default.errorMessages,
    units = _configuration2.default.units;

var Soldier = function (_Unit) {
    _inherits(Soldier, _Unit);

    function Soldier(recharge) {
        _classCallCheck(this, Soldier);

        var _this = _possibleConstructorReturn(this, (Soldier.__proto__ || Object.getPrototypeOf(Soldier)).call(this, recharge));

        _this.experience = soldiers.experience.min;
        return _this;
    }

    _createClass(Soldier, [{
        key: 'getAttackProbability',
        value: function getAttackProbability() {
            var probability = 0.5 * (1 + this.health / 100) * methods.random(30 + this.experience, 100);
            return probability / 100;
        }
    }, {
        key: 'getAttackingDamage',
        value: function getAttackingDamage() {
            return (5 + this.experience) / 100;
        }
    }, {
        key: 'attack',
        value: function attack(unit) {
            var that = this;
            console.log('Soldier attacking');
            return new Promise(function (resolve) {
                var attacking_probability = that.getAttackProbability();
                var defending_probability = unit.getAttackProbability();
                console.log('Attacking probability ' + attacking_probability);
                console.log('Defending probability ' + defending_probability);
                if (attacking_probability > defending_probability) {
                    var damage = that.getAttackingDamage();
                    console.log('Attack successful, unit damaged with ' + damage);
                    unit.takeDamage(damage);
                    that.experience += 1;
                    setTimeout(function () {
                        resolve(true);
                    }, that.recharge);
                } else {
                    console.log('Attack failed');
                    resolve(false);
                }
            });
        }
    }, {
        key: 'experience',
        get: function get() {
            return this._experience;
        },
        set: function set(experience) {
            if ((typeof experience === 'undefined' ? 'undefined' : _typeof(experience)) != soldiers.experience['type']) {
                throw new Error('Wrong argument type for experience: ' + errorMessages.expected(soldiers.experience['type'], experience));
            }
            if (experience < soldiers.experience.min) {
                throw new Error(errorMessages.with('experience') + ' ' + errorMessages.min + ' : ' + errorMessages.expected('greater then ' + soldiers.experience.min, experience));
            }
            if (experience > soldiers.experience.max) {
                experience = soldiers.experience.max;
            }
            this._experience = experience;
        }
    }]);

    return Soldier;
}(_unit2.default);

exports.default = Soldier;