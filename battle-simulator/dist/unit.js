'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _configuration = require('./configuration');

var _configuration2 = _interopRequireDefault(_configuration);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var units = _configuration2.default.units,
    errorMessages = _configuration2.default.errorMessages;

var Unit = function () {
    function Unit(recharge) {
        _classCallCheck(this, Unit);

        this.health = units.health.max;
        this.recharge = recharge;
    }

    _createClass(Unit, [{
        key: 'takeDamage',
        value: function takeDamage(damage) {
            console.log('Taking ' + damage + ' damage. From health ' + this.health + ' ');
            if ((typeof damage === 'undefined' ? 'undefined' : _typeof(damage)) != units.health['type']) {
                throw new Error(errorMessages.with('damage property') + ': ' + errorMessages.expected(units.health['type'], typeof damage === 'undefined' ? 'undefined' : _typeof(damage)));
            }
            this.health -= damage;
            console.log('to ' + this.health + '.');
        }
    }, {
        key: 'isDestroyed',
        value: function isDestroyed() {
            return this.health <= 0;
        }
    }, {
        key: 'health',
        get: function get() {
            return this._health;
        },
        set: function set(health) {
            if ((typeof health === 'undefined' ? 'undefined' : _typeof(health)) != units.health['type']) {
                throw new Error('Wrong argument type for health: ' + errorMessages.expected(units.health['type'], health));
            }
            if (health < units.health.min) {
                health = units.health.min;
            }
            if (health > units.health.max) {
                throw new Error('health ' + errorMessages.max + ' : ' + errorMessages.expected('less then ' + units.health.max, health));
            }
            this._health = health;
        }
    }, {
        key: 'recharge',
        get: function get() {
            return this._recharge;
        },
        set: function set(recharge) {
            if ((typeof recharge === 'undefined' ? 'undefined' : _typeof(recharge)) != units.recharge['type']) {
                throw new Error('Wrong argument type for recharge: ' + errorMessages.expected(units.recharge['type'], recharge));
            }
            if (recharge < units.recharge.min(true)) {
                throw new Error(errorMessages.with('health') + ' ' + errorMessages.min + ' : ' + errorMessages.expected('greater then ' + units.recharge.min(true), recharge));
            }
            if (recharge > units.recharge.max) {
                throw new Error('health ' + errorMessages.max + ' : ' + errorMessages.expected('less then ' + units.recharge.max, recharge));
            }
            this._recharge = recharge;
        }
    }]);

    return Unit;
}();

exports.default = Unit;