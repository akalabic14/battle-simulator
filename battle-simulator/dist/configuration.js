'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

/**
 * @description
 * restrictions for arguments 
 */
var reading_arguments = {
    arguments: {
        number_of_armies: {
            type: 'number',
            min: 2
        },
        attack_strategies: {
            type: 'array',
            min: 2
        },
        attack_strategy: {
            type: 'string',
            options: ['random', 'weakest', 'strongest']
        },
        squads_per_army: {
            type: 'number',
            min: 2
        },
        units_per_squad: {
            type: 'number',
            min: 5,
            max: 10
        }
    }
},

/**
 * @description
 * Some generic error messages
 */
errorMessages = {
    readme: function readme(got) {
        return 'Check README file for more details about ' + got;
    },
    expected: function expected(exp, got) {
        return 'Expected ' + exp + ', but got ' + got;
    },
    min: 'value smaller than min allowed',
    max: 'value greater than max allowed',
    with: function _with(got) {
        return 'Error with ' + got + ':';
    }
},

/**
 * @description
 * restrictions for units arguments
 */
units = {
    health: {
        min: 0,
        max: 100,
        type: 'number'
    },
    recharge: {
        min: function min(soldier) {
            return soldier ? 200 : 1000;
        },
        max: 2000,
        type: 'number'
    }
},
    soldiers = {
    experience: {
        min: 0,
        max: 50,
        type: 'number'
    }
},
    vehicles = {
    operators: {
        min: 1,
        max: 3
    }
},
    methods = {
    random: function random(min, max) {
        return Math.floor(Math.random() * (max - min) + min);
    },
    avarage: function avarage(array) {
        return array.reduce(function (a, b) {
            return a + b;
        }, 0) / array.length;
    },
    gavg: function gavg(array) {
        return Math.pow(array.reduce(function (a, b) {
            return a * b;
        }, 1), 1 / array.length);
    }
};

exports.default = { reading_arguments: reading_arguments, errorMessages: errorMessages, units: units, soldiers: soldiers, methods: methods, vehicles: vehicles };