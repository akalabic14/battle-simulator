Battle simulator written for Trade Core as first step of back-end developer interview.

The Battlefield will support a configurable number of armies. Each army can have a configurable number of squads. Each squad consisted of a number of units.
Once the simulator is started all army squads will start attacking each other until there is only one army left.

Configurable contstrains are written in `configurable.js` file, and can be updated to change the battle parameters.
Constrains are :
    - number_of_armies: 2 <= n
    - attack_strategies: [attack_strategy_1, attack_strategy_2, ... , attack_strategy_n] - where attack_strategy is a string random|weakest|strongest
    - squads_per_army: 2 <= n
    - units_per_squad: 5 <= n <= 10 

To start simulation run: npm run start
To start provided unit tests run: npm test

This app will log you all of the battles, and finally the winner.