'use strict';

var _readArguments = require('./read-arguments');

var _readArguments2 = _interopRequireDefault(_readArguments);

var _configurable = require('./configurable');

var _configurable2 = _interopRequireDefault(_configurable);

var _army = require('./army');

var _army2 = _interopRequireDefault(_army);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

console.log("Wellcome to Battle Simulator!");

_readArguments2.default.read(_configurable2.default);

console.log("Arguments successfully loaded.");
var armies = [];
var wars = [];
for (var i = 0; i < _configurable2.default.number_of_armies; i++) {
    armies.push(new _army2.default(_configurable2.default.attack_strategies[i]));
}
for (var i = 0; i < _configurable2.default.number_of_armies; i++) {
    wars.push(new Promise(function (resolve) {
        armies[i].war(armies.splice(i, 1)).then(function (response) {
            if (response == 'Victory') {
                console.log('Army ' + i + '. has won the war!');
            }
        });
    }));
};
Promise.all(wars).then(function (response) {
    console.log('Peace has come.');
});