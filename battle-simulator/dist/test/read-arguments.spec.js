'use strict';

var chai = require('chai');
var expect = chai.expect;
var errorMessages = require('../dist/configuration').default.errorMessages;
var reading_arguments = require('../dist/configuration').default.reading_arguments;
var read = require('../dist/read-arguments').default.read;

describe('Reading arguments from configurable file', function () {
    it('Should return error when passed empty object', function () {
        var result = read({});
        expect(result).to.be.an('error');
    });
    it('Should return error when passed random object', function () {
        var result = read({ 'foo': 'boo' });
        expect(result).to.be.an('error');
    });
    it('Should return error when passed array of numbers', function () {
        var result = read([3, 4, 0, 2]);
        expect(result).to.be.an('error');
    });
    it('Should return error when number_of_armies is less then minimum', function () {
        var object = {
            'number_of_armies': reading_arguments.arguments['number_of_armies'].min - 1,
            'attack_strategies': [reading_arguments.arguments['attack_strategy'].options[0], reading_arguments.arguments['attack_strategy'].options[0]],
            'squads_per_army': reading_arguments.arguments['squads_per_army'].min,
            'units_per_squad': reading_arguments.arguments['units_per_squad'].min
        };
        var result = read(object);
        expect(result).to.be.an('error');
    });
    it('Should return error when number_of_armies is a string', function () {
        var object = {
            'number_of_armies': 'one',
            'attack_strategies': [reading_arguments.arguments['attack_strategy'].options[0], reading_arguments.arguments['attack_strategy'].options[0]],
            'squads_per_army': reading_arguments.arguments['squads_per_army'].min,
            'units_per_squad': reading_arguments.arguments['units_per_squad'].min
        };
        var result = read(object);
        expect(result).to.be.an('error');
    });
    it('Should return error when number_of_armies is a boolean', function () {
        var object = {
            'number_of_armies': true,
            'attack_strategies': [reading_arguments.arguments['attack_strategy'].options[0], reading_arguments.arguments['attack_strategy'].options[0]],
            'squads_per_army': reading_arguments.arguments['squads_per_army'].min,
            'units_per_squad': reading_arguments.arguments['units_per_squad'].min
        };
        var result = read(object);
        expect(result).to.be.an('error');
    });
    it('Should return error when attack_strategy is neither random, weakest or strongest', function () {
        var object = {
            'number_of_armies': reading_arguments.arguments['number_of_armies'].min,
            'attack_strategies': ['toughest', reading_arguments.arguments['attack_strategy'].options[0]],
            'squads_per_army': reading_arguments.arguments['squads_per_army'].min,
            'units_per_squad': reading_arguments.arguments['units_per_squad'].min
        };
        var result = read(object);
        expect(result).to.be.an('error');
    });
    it('Should return error when attack_strategies array is empty array', function () {
        var object = {
            'number_of_armies': reading_arguments.arguments['number_of_armies'].min + 3,
            'attack_strategies': [],
            'squads_per_army': reading_arguments.arguments['squads_per_army'].min + 3,
            'units_per_squad': reading_arguments.arguments['units_per_squad'].max
        };
        var result = read(object);
        expect(result).to.be.an('error');
    });
    it('Should return error when attack_strategies array is shorter than number_of_armies', function () {
        var object = {
            'number_of_armies': reading_arguments.arguments['number_of_armies'].min + 3,
            'attack_strategies': [reading_arguments.arguments['attack_strategy'].options[0]],
            'squads_per_army': reading_arguments.arguments['squads_per_army'].min + 3,
            'units_per_squad': reading_arguments.arguments['units_per_squad'].max
        };
        var result = read(object);
        expect(result).to.be.an('error');
    });
    it('Should return error when attack_strategies array is longer than number_of_armies', function () {
        var object = {
            'number_of_armies': reading_arguments.arguments['number_of_armies'].min + 3,
            'attack_strategies': [reading_arguments.arguments['attack_strategy'].options[0], reading_arguments.arguments['attack_strategy'].options[0], reading_arguments.arguments['attack_strategy'].options[0]],
            'squads_per_army': reading_arguments.arguments['squads_per_army'].min + 3,
            'units_per_squad': reading_arguments.arguments['units_per_squad'].max
        };
        var result = read(object);
        expect(result).to.be.an('error');
    });
    it('Should return error when attack_strategy is boolean', function () {
        var object = {
            'number_of_armies': reading_arguments.arguments['number_of_armies'].min,
            'attack_strategies': [true, reading_arguments.arguments['attack_strategy'].options[0]],
            'squads_per_army': reading_arguments.arguments['squads_per_army'].min,
            'units_per_squad': reading_arguments.arguments['units_per_squad'].min
        };
        var result = read(object);
        expect(result).to.be.an('error');
    });
    it('Should return error when attack_strategy is number', function () {
        var object = {
            'number_of_armies': reading_arguments.arguments['number_of_armies'].min,
            'attack_strategies': [13, reading_arguments.arguments['attack_strategy'].options[0]],
            'squads_per_army': reading_arguments.arguments['squads_per_army'].min,
            'units_per_squad': reading_arguments.arguments['units_per_squad'].min
        };
        var result = read(object);
        expect(result).to.be.an('error');
    });
    it('Should return error when squads_per_army is less then minimum', function () {
        var object = {
            'number_of_armies': reading_arguments.arguments['number_of_armies'].min,
            'attack_strategies': [reading_arguments.arguments['attack_strategy'].options[0], reading_arguments.arguments['attack_strategy'].options[0]],
            'squads_per_army': reading_arguments.arguments['squads_per_army'].min - 1,
            'units_per_squad': reading_arguments.arguments['units_per_squad'].min
        };
        var result = read(object);
        expect(result).to.be.an('error');
    });
    it('Should return error when squads_per_army is a string', function () {
        var object = {
            'number_of_armies': reading_arguments.arguments['number_of_armies'].min,
            'attack_strategies': [reading_arguments.arguments['attack_strategy'].options[0], reading_arguments.arguments['attack_strategy'].options[0]],
            'squads_per_army': 'two',
            'units_per_squad': reading_arguments.arguments['units_per_squad'].min
        };
        var result = read(object);
        expect(result).to.be.an('error');
    });
    it('Should return error when squads_per_army is boolean', function () {
        var object = {
            'number_of_armies': reading_arguments.arguments['number_of_armies'].min,
            'attack_strategies': [reading_arguments.arguments['attack_strategy'].options[0], reading_arguments.arguments['attack_strategy'].options[0]],
            'squads_per_army': true,
            'units_per_squad': reading_arguments.arguments['units_per_squad'].min
        };
        var result = read(object);
        expect(result).to.be.an('error');
    });
    it('Should return error when units_per_squad is less then minimum', function () {
        var object = {
            'number_of_armies': reading_arguments.arguments['number_of_armies'].min,
            'attack_strategies': [reading_arguments.arguments['attack_strategy'].options[0], reading_arguments.arguments['attack_strategy'].options[0]],
            'squads_per_army': reading_arguments.arguments['squads_per_army'].min,
            'units_per_squad': reading_arguments.arguments['units_per_squad'].min - 1
        };
        var result = read(object);
        expect(result).to.be.an('error');
    });
    it('Should return error when units_per_squad is a string', function () {
        var object = {
            'number_of_armies': reading_arguments.arguments['number_of_armies'].min,
            'attack_strategies': [reading_arguments.arguments['attack_strategy'].options[0], reading_arguments.arguments['attack_strategy'].options[0]],
            'squads_per_army': reading_arguments.arguments['squads_per_army'].min,
            'units_per_squad': 'four'
        };
        var result = read(object);
        expect(result).to.be.an('error');
    });
    it('Should return error when units_per_squad is a boolean', function () {
        var object = {
            'number_of_armies': reading_arguments.arguments['number_of_armies'].min,
            'attack_strategies': [reading_arguments.arguments['attack_strategy'].options[0], reading_arguments.arguments['attack_strategy'].options[0]],
            'squads_per_army': reading_arguments.arguments['squads_per_army'].min,
            'units_per_squad': true
        };
        var result = read(object);
        expect(result).to.be.an('error');
    });
    it('Should return error when units_per_squad is greater then maximum', function () {
        var object = {
            'number_of_armies': reading_arguments.arguments['number_of_armies'].min,
            'attack_strategies': [reading_arguments.arguments['attack_strategy'].options[0], reading_arguments.arguments['attack_strategy'].options[0]],
            'squads_per_army': reading_arguments.arguments['squads_per_army'].min,
            'units_per_squad': reading_arguments.arguments['units_per_squad'].max + 1
        };
        var result = read(object);
        expect(result).to.be.an('error');
    });
    it('Should return true with default values when minimum values are passed (attack_strategy random)', function () {
        var object = {
            'number_of_armies': reading_arguments.arguments['number_of_armies'].min,
            'attack_strategies': [reading_arguments.arguments['attack_strategy'].options[0], reading_arguments.arguments['attack_strategy'].options[0]],
            'squads_per_army': reading_arguments.arguments['squads_per_army'].min,
            'units_per_squad': reading_arguments.arguments['units_per_squad'].min
        };
        var result = read(object);
        expect(result).to.be.an('boolean');
    });
    it('Should return true with default values when (min + 4) valid values are passed (attack_strategy weakest)', function () {
        var object = {
            'number_of_armies': reading_arguments.arguments['number_of_armies'].min,
            'attack_strategies': [reading_arguments.arguments['attack_strategy'].options[1], reading_arguments.arguments['attack_strategy'].options[1]],
            'squads_per_army': reading_arguments.arguments['squads_per_army'].min + 4,
            'units_per_squad': reading_arguments.arguments['units_per_squad'].min + 4
        };
        var result = read(object);
        expect(result).to.be.an('boolean');
    });
    it('Should return object with default values when valid values (min + 2) are passed (attack_strategy strongest)', function () {
        var object = {
            'number_of_armies': reading_arguments.arguments['number_of_armies'].min,
            'attack_strategies': [reading_arguments.arguments['attack_strategy'].options[2], reading_arguments.arguments['attack_strategy'].options[2]],
            'squads_per_army': reading_arguments.arguments['squads_per_army'].min + 2,
            'units_per_squad': reading_arguments.arguments['units_per_squad'].min
        };
        var result = read(object);
        expect(result).to.be.an('boolean');
    });
    it('Should return object with default values when valid values (min + 3) are passed (units_per_squad max)', function () {
        var object = {
            'number_of_armies': reading_arguments.arguments['number_of_armies'].min,
            'attack_strategies': [reading_arguments.arguments['attack_strategy'].options[0], reading_arguments.arguments['attack_strategy'].options[0]],
            'squads_per_army': reading_arguments.arguments['squads_per_army'].min + 3,
            'units_per_squad': reading_arguments.arguments['units_per_squad'].max
        };
        var result = read(object);
        expect(result).to.be.an('boolean');
    });
});