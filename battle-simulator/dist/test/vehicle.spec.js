'use strict';

var chai = require('chai');
var Vehicle = require('../dist/vehicle').default;
var configuration = require('../dist/configuration').default;
var expect = chai.expect;
var units_config = configuration.units;
var vehicle_config = configuration.vehicles;

describe('Testing Vehicle class methods', function () {
    it('Should return instanceof Vehicle when initiated with correct values', function () {
        var recharge = configuration.methods.random(units_config.recharge.min(false), units_config.recharge.max);
        var test_vehicle = new Vehicle(recharge);
        expect(test_vehicle instanceof Vehicle).to.equal(true);
    });
    it('Should throw error when recharge property is less then ' + units_config.recharge.min(false), function () {
        expect(function () {
            var test_vehicle = new Vehicle(units_config.recharge.min(false) - 1);
        }).to.Throw(Error);
    });
    it('Vehicle should have between ' + vehicle_config.operators.min + ' and ' + vehicle_config.operators.max + ' operators after instancing', function () {
        var recharge = configuration.methods.random(units_config.recharge.min(false), units_config.recharge.max);
        var test_vehicle = new Vehicle(recharge);
        expect(test_vehicle.operators.length >= vehicle_config.operators.min && test_vehicle.operators.length <= vehicle_config.operators.max).to.equal(true);
    });
    it('Health should be at maximum after creating', function () {
        var recharge = configuration.methods.random(units_config.recharge.min(false), units_config.recharge.max);
        var test_vehicle = new Vehicle(recharge);
        expect(test_vehicle.health).to.equal(units_config.health.max);
    });
    it('getRealHealth should return sum of the average health of all its operators and the health of the vehicle.', function () {
        var recharge = configuration.methods.random(units_config.recharge.min(false), units_config.recharge.max);
        var test_vehicle = new Vehicle(recharge);
        var health = test_vehicle.health + configuration.methods.avarage(test_vehicle.operators.map(function (operator) {
            return operator.health;
        }));

        expect(test_vehicle.getRealHealth()).to.equal(health);
    });
    it('Health value should be reduced by 0.3 * attackers calculated damage after successful attack.', function (done) {
        var test_vehicle_1 = new Vehicle(units_config.recharge.min(false));
        var test_vehicle_2 = new Vehicle(units_config.recharge.min(false));
        var old_health = test_vehicle_2.health;
        var damage = test_vehicle_2.operators.length > 1 ? parseFloat(parseInt(test_vehicle_1.getAttackingDamage() * 30) / 100) : parseFloat(parseInt(test_vehicle_1.getAttackingDamage() * 50) / 100);
        test_vehicle_1.attack(test_vehicle_2).then(function (response) {
            expect(test_vehicle_2.health).to.equal(response ? old_health - damage : old_health);
            done();
        });
    });
    it('isDestroyed should be true after attackickng while there are no operators in vehicle.', function (done) {
        async function destroy(vehicle_1, unit) {
            return new Promise(function (resolve) {
                vehicle_1.attack(unit).then(function (successful) {
                    if (unit.isDestroyed()) {
                        resolve(true);
                    } else {
                        resolve(destroy(vehicle_1, unit));
                    }
                });
            });
        }
        var test_vehicle_1 = new Vehicle(units_config.recharge.min(false));
        var test_vehicle_2 = new Vehicle(units_config.recharge.min(false));
        var old_health = test_vehicle_2.health;
        var damage = test_vehicle_2.operators.length > 1 ? parseFloat(parseInt(test_vehicle_1.getAttackingDamage() * 30) / 100) : parseFloat(parseInt(test_vehicle_1.getAttackingDamage() * 50) / 100);
        destroy(test_vehicle_1, test_vehicle_2).then(function (response) {
            expect(test_vehicle_2.isDestroyed()).to.equal(true);
            done();
        });
    });
});