'use strict';

var chai = require('chai');
var Squad = require('../dist/squad').default;
var configuration = require('../dist/configuration').default;
var configurable = require('../dist/configurable').default;
var expect = chai.expect;
var arguments_config = configuration.reading_arguments;

describe('Testing Squad class methods', function () {
    it('Should return instanceof Squad when initiated with correct values', function () {
        var test_squad = new Squad(arguments_config.arguments.attack_strategy.options[0]);
        expect(test_squad instanceof Squad).to.equal(true);
    });
    it('Squad should have ' + configurable.units_per_squad + ' units after instancing', function () {
        var test_squad = new Squad(arguments_config.arguments.attack_strategy.options[0]);
        expect(test_squad.units.length).to.equal(configurable.units_per_squad);
    });
    it('Attack strategy should be the one we passed while creating', function () {
        var random = configuration.methods.random(0, arguments_config.arguments.attack_strategy.options.length);
        var strategy = arguments_config.arguments.attack_strategy.options[random];
        var test_squad = new Squad(strategy);
        expect(test_squad.attack_strategy).to.equal(strategy);
    });
    it('Should throw error when passed invalid attack_strategy', function () {
        expect(function () {
            var throwing = new Squad('foo');
        }).to.Throw(Error);
    });
});