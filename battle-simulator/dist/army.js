'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _squad = require('./squad');

var _squad2 = _interopRequireDefault(_squad);

var _configurable = require('./configurable');

var _configurable2 = _interopRequireDefault(_configurable);

var _configuration = require('./configuration');

var _configuration2 = _interopRequireDefault(_configuration);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var methods = _configuration2.default.methods;

var Army = function () {
    function Army(attack_strategy) {
        _classCallCheck(this, Army);

        this.squads = [];
        for (var i = 0; i < _configurable2.default.squads_per_army; i++) {
            this.squads.push(new _squad2.default(attack_strategy));
        }
    }

    _createClass(Army, [{
        key: 'attack',
        value: function attack(opponents) {
            var all_squads = [];
            var squads_hash = {};
            var attacks_array = [];
            opponents.forEach(function (army, i) {
                army.squads.forEach(function (squad, j) {
                    squads_hash[squad] = { army: army, index: j };
                    all_squads.push(squad);
                });
            });
            this.squads.forEach(function (squad) {
                var choice = void 0;
                switch (squad.attack_strategy) {
                    case 'random':
                        choice = methods.random(0, all_squads.length);
                        break;
                    case 'weakest':
                        choice = all_squads.indexOf(this.sortSquads(all_squads)[0]);
                        break;
                    case 'strongest':
                        choice = all_squads.indexOf(this.sortSquads(all_squads)[all_squads.length - 1]);
                        break;
                }
                attacks_array.push(squad.attack(all_squads[choice]).then(function (response) {
                    if (all_squads[choice].isDestroyed()) {
                        var opponent_army = squads_hash[all_squads[choice]];
                        opponents_army.army.squad.splice(opponent_army.index, 1);
                    }
                }));
            }, this);
            return Promise.all(attacks_array);
        }
    }, {
        key: 'isDestroyed',
        value: function isDestroyed() {
            return this.squads.length > 0;
        }
    }, {
        key: 'war',
        value: function (_war) {
            function war(_x) {
                return _war.apply(this, arguments);
            }

            war.toString = function () {
                return _war.toString();
            };

            return war;
        }(function (opponents) {
            this.attack(opponents).then(function (response) {
                opponents.forEach(function (opponent, i) {
                    if (opponent.isDestroyed()) {
                        opponents.splice(i, 1);
                    }
                    if (opponents.length > 0) {
                        war(opponents);
                    } else {
                        return 'Victory!';
                    }
                });
            });
        })
    }, {
        key: 'sortSquads',
        value: function sortSquads(squads) {
            return squads.sort(function (squad_1, squad_2) {
                if (squad_1.getTotalSquadHealth() > squad_2.getTotalSquadHealth()) {
                    return 1;
                }
                if (squad_1.getTotalSquadHealth() < squad_2.getTotalSquadHealth()) {
                    return -1;
                }
                if (squad_1.getSquadsExperiance() > squad_2.getSquadsExperiance()) {
                    return 1;
                }
                if (squad_1.getSquadsExperiance() < squad_2.getSquadsExperiance()) {
                    return -1;
                }
                if (squad_1.units.length > squad_2.units.length) {
                    return 1;
                }
                if (squad_1.units.length < squad_2.units.length) {
                    return -1;
                }
                if (squad_1.getTotalSquadDamage() > squad_2.getTotalSquadDamage()) {
                    return 1;
                }
                if (squad_1.getTotalSquadDamage() < squad_2.getTotalSquadDamage()) {
                    return -1;
                }
                return 0;
            });
        }
    }, {
        key: 'squads',
        get: function get() {
            return this._squads;
        },
        set: function set(squads) {
            this._squads = squads;
        }
    }]);

    return Army;
}();

exports.default = Army;