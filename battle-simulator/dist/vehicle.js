'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _unit = require('./unit');

var _unit2 = _interopRequireDefault(_unit);

var _soldier = require('./soldier');

var _soldier2 = _interopRequireDefault(_soldier);

var _configuration = require('./configuration');

var _configuration2 = _interopRequireDefault(_configuration);

var _configurable = require('./configurable');

var _configurable2 = _interopRequireDefault(_configurable);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var methods = _configuration2.default.methods,
    vehicles = _configuration2.default.vehicles,
    units = _configuration2.default.units,
    errorMessages = _configuration2.default.errorMessages,
    soldiers = _configuration2.default.soldiers;

var Vehicle = function (_Unit) {
    _inherits(Vehicle, _Unit);

    function Vehicle(recharge) {
        _classCallCheck(this, Vehicle);

        if (recharge < units.recharge.min(false)) {
            throw new Error(errorMessages.with('recharge') + ' ' + errorMessages.min + ' : ' + errorMessages.expected('greater then ' + units.recharge.min(false), recharge));
        }

        var _this = _possibleConstructorReturn(this, (Vehicle.__proto__ || Object.getPrototypeOf(Vehicle)).call(this, recharge));

        _this.operators = [];
        for (var i = 0, length = methods.random(vehicles.operators.min, vehicles.operators.max); i < length; i++) {
            var _recharge = methods.random(units.recharge.min(true), units.recharge.max);
            _this.operators.push(new _soldier2.default(_recharge));
        }
        return _this;
    }

    _createClass(Vehicle, [{
        key: 'getRealHealth',
        value: function getRealHealth() {
            this.operators.forEach(function (operator, i) {
                console.log('Unit ' + i + '. health: ' + operator.health);
            });
            return this.health + methods.avarage(this.operators.map(function (operator) {
                return operator.health;
            }));
        }
    }, {
        key: 'isDestroyed',
        value: function isDestroyed() {
            return this.health <= 0 || this.getRealHealth() <= 0;
        }
    }, {
        key: 'getAttackProbability',
        value: function getAttackProbability() {
            return 0.5 * ((100 + this.health) / 100) * methods.gavg(this.operators.map(function (operator) {
                return operator.getAttackProbability();
            }));
        }
    }, {
        key: 'getAttackingDamage',
        value: function getAttackingDamage() {
            return (10 + this.operators.reduce(function (accumulator, operator) {
                return accumulator + operator.experience;
            }, 0)) / 100;
        }
    }, {
        key: 'takeDamage',
        value: function takeDamage(damage) {
            console.log('Vehicle taking ' + damage * 30 / 100 + ' damage.');
            var vehicle_damage = damage * 30 / 100;
            var random_operator_damage = damage / 2;
            damage = parseFloat(parseInt(damage * 100 - vehicle_damage * 100 - random_operator_damage * 100) / 100);
            this.health -= vehicle_damage;
            var random_operator = methods.random(0, this.operators.length - 1);
            console.log('Operator ' + random_operator + ' taking ' + random_operator_damage + ' damage.');
            this.operators[random_operator].takeDamage(random_operator_damage);
            if (this.operators[random_operator].isDestroyed()) {
                console.log('Operator ' + random_operator + ' destroyed.');
                this.operators.splice(random_operator, 1);
                random_operator = this.operators.length;
            }
            if (this.operators.length > 1 || random_operator != 0) {
                var avarage = damage / (this.operators.length - 1);
                this.operators.forEach(function (operator, i) {
                    if (i != random_operator) {
                        console.log('Operator ' + i + ' taking ' + avarage + ' damage.');
                        operator.takeDamage(avarage);
                        if (operator.isDestroyed()) {
                            console.log('Operator ' + random_operator + ' destroyed.');
                            this.operators.splice(i, 1);
                        }
                    }
                }, this);
            } else {
                console.log('Vehicle taking ' + damage + ' damage, hence there are no operators left.');
                this.health -= damage;
            }
        }
    }, {
        key: 'attack',
        value: function attack(unit) {
            var _this2 = this;

            var that = this;
            return new Promise(function (resolve) {
                console.log('Vehicle attacking');
                var attacking_probability = that.getAttackProbability();
                var defending_probability = unit.getAttackProbability();
                console.log('Attacking probability ' + attacking_probability);
                console.log('Defending probability ' + defending_probability);
                if (attacking_probability > defending_probability) {
                    var damage = that.getAttackingDamage();
                    console.log('Attack successful, unit damaged with ' + damage);
                    unit.takeDamage(that.getAttackingDamage());
                    that.operators.forEach(function (operator) {
                        operator.experience += 1;
                    }, _this2);
                    setTimeout(function () {
                        resolve(true);
                    }, that.recharge);
                } else {
                    resolve(false);
                }
            });
        }
    }, {
        key: 'operators',
        get: function get() {
            return this._operators;
        },
        set: function set(operators) {
            this._operators = operators;
        }
    }]);

    return Vehicle;
}(_unit2.default);

exports.default = Vehicle;