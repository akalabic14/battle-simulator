import configuration from './configuration'

const {units, errorMessages} = configuration

class Unit {
    constructor(recharge) {
        this.health = units.health.max
        this.recharge = recharge
    }

    get health() {
        return this._health
    }

    set health(health) {
        if (typeof health != units.health['type']) {
            throw new Error(`Wrong argument type for health: ${errorMessages.expected(units.health['type'], health)}`)
        }
        if (health < units.health.min) {
            health = units.health.min
        }
        if (health > units.health.max) {
            throw new Error(`health ${errorMessages.max} : ${errorMessages.expected(`less then ${units.health.max}`, health)}`)
        }
        this._health = health
    }

    get recharge() {
        return this._recharge
    }

    set recharge(recharge) {
        if (typeof recharge != units.recharge['type']) {
            throw new Error(`Wrong argument type for recharge: ${errorMessages.expected(units.recharge['type'], recharge)}`)
        }
        if (recharge < units.recharge.min(true)) {
            throw new Error(`${errorMessages.with('health')} ${errorMessages.min} : ${errorMessages.expected(`greater then ${units.recharge.min(true)}`, recharge)}`)
        }
        if (recharge > units.recharge.max) {
            throw new Error(`health ${errorMessages.max} : ${errorMessages.expected(`less then ${units.recharge.max}`, recharge)}`)
        }
        this._recharge = recharge
    }

    takeDamage(damage) {
        console.log(`Taking ${damage} damage. From health ${this.health} `);
        if (typeof damage != units.health['type']) {
            throw new Error(`${errorMessages.with('damage property')}: ${errorMessages.expected(units.health['type'], typeof damage)}`)
        }
        this.health -= damage
        console.log(`to ${this.health}.`);
    }

    isDestroyed() {
        return this.health <= 0
    }
}

export default Unit