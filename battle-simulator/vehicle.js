import Unit from './unit'
import Soldier from './soldier'
import configuration from './configuration'
import configurable from './configurable'

const {methods, vehicles, units, errorMessages, soldiers} = configuration;

class Vehicle extends Unit {
    constructor(recharge) {
        if(recharge < units.recharge.min(false)) {
            throw new Error(`${errorMessages.with('recharge')} ${errorMessages.min} : ${errorMessages.expected(`greater then ${units.recharge.min(false)}`, recharge)}`)
        }
        super(recharge)
        this.operators = []
        for(var i=0, length=methods.random(vehicles.operators.min, vehicles.operators.max); i<length; i++) {
            let recharge = methods.random(units.recharge.min(true), units.recharge.max)
            this.operators.push(new Soldier(recharge));
        }
    }

    get operators() {
        return this._operators
    }

    set operators(operators) {
        this._operators = operators
    }
    getRealHealth() {
        this.operators.forEach(function(operator, i) {
            console.log(`Unit ${i}. health: ${operator.health}`)
        })
        return this.health + methods.avarage(this.operators.map(operator => operator.health))
    }

    isDestroyed() {
        return this.health <= 0 || this.getRealHealth() <= 0
    }

    getAttackProbability () {
        return 0.5 * ((100 + this.health) / 100) * methods.gavg(this.operators.map(operator => operator.getAttackProbability()))
    }

    getAttackingDamage () {
        return (10 + this.operators.reduce((accumulator, operator) => accumulator + operator.experience, 0)) / 100
    }

    takeDamage(damage) {
        console.log(`Vehicle taking ${damage*30/100} damage.`);
        let vehicle_damage = damage * 30 / 100;
        let random_operator_damage = damage / 2;
        damage = parseFloat(parseInt(damage * 100 - vehicle_damage * 100 - random_operator_damage * 100) / 100)
        this.health -= vehicle_damage
        let random_operator = methods.random(0,this.operators.length - 1)
        console.log(`Operator ${random_operator} taking ${random_operator_damage} damage.`)
        this.operators[random_operator].takeDamage(random_operator_damage)
        if (this.operators[random_operator].isDestroyed()) {
            console.log(`Operator ${random_operator} destroyed.`)
            this.operators.splice(random_operator, 1)
            random_operator = this.operators.length
        }
        if(this.operators.length > 1 || random_operator != 0) {
            let avarage = damage / (this.operators.length - 1)
            this.operators.forEach(function(operator, i) {
                if (i != random_operator) {
                    console.log(`Operator ${i} taking ${avarage} damage.`)
                    operator.takeDamage(avarage)
                    if (operator.isDestroyed()) {
                        console.log(`Operator ${random_operator} destroyed.`)
                        this.operators.splice(i, 1)
                    }
                }
            }, this);
        } else {
            console.log(`Vehicle taking ${damage} damage, hence there are no operators left.`);
            this.health -= damage
        }
    }

    attack(unit) {
        var that = this
        return new Promise((resolve) => {
            console.log('Vehicle attacking')
            let attacking_probability = that.getAttackProbability()
            let defending_probability = unit.getAttackProbability()
            console.log(`Attacking probability ${attacking_probability}`)
            console.log(`Defending probability ${defending_probability}`)
            if (attacking_probability > defending_probability) {
                let damage = that.getAttackingDamage()
                console.log(`Attack successful, unit damaged with ${damage}`)
                unit.takeDamage(that.getAttackingDamage())
                that.operators.forEach(function(operator) {
                    operator.experience += 1
                }, this);
                setTimeout(() => {
                    resolve(true)
                }, that.recharge)
            } else {
                resolve(false);
            }
        })
    }
}

export default Vehicle